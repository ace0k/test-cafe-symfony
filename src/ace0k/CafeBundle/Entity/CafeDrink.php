<?php

namespace ace0k\CafeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ace0k\CafeBundle\Entity\CafeItem;


/**
 * Class that will be used for Drinks
 *
 * @ORM\Entity
 * @ORM\Table(name="CafeDrink")
 */
class CafeDrink extends CafeItem
{

    /**
     * What an item consist of
     *
     * @var array
     * @ORM\Column(type="array")
     */
    private $recipe = array();

    /**
     * item constructor.
     *
     * @param $name
     * @param $description
     * @param $recipe
     */
    public function __construct($name, $description, $recipe)
    {
        $total_price = 0;
        foreach ($recipe as $item) {
            if ($item instanceof CafeItem) {
                $id = $item->getId();
                $total_price += $item->getPrice();
                if (!isset($this->recipe[$id])) {
                    $this->recipe[ $id ] = $item;
                }
            }
        }
        parent::__construct($name, $description, $total_price);
    }

    /**
     * Set recipe
     *
     * @param array $recipe
     *
     * @return CafeDrink
     */
    public function setRecipe($recipe)
    {
        
        foreach ($recipe as $item) {
            if ($item instanceof CafeItem) {
                $id = $item->getId();
                if (!isset($this->recipe[$id])) {
                    $this->recipe[ $id ] = $item;
                }
            }
        }

        return $this;
    }

    /**
     * Add item to recipe
     *
     * @param array $item
     *
     * @return CafeDrink
     */
    public function addConsumable($item)
    {
        if ($item instanceof CafeItem) {
            $id = $item->getId();
            if (!isset($this->recipe[$id])) {
                $this->recipe[$id] = $item;
            }
        }

        return $this;
    }

    /**
     * Add item to recipe
     *
     * @return CafeDrink
     */
    public function removeConsumable($id)
    {
        if (isset($this->recipe[$id])) {
            unset($this->recipe[$id]);
        }

        return $this;
    }

    /**
     * Get recipe
     *
     * @return array
     */
    public function getRecipe()
    {
        return $this->recipe;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
