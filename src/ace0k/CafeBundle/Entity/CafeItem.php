<?php

namespace ace0k\CafeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class that can be used to create Drink objects with few parameters
 *
 * @ORM\Entity
 * @ORM\Table(name="CafeItem")
 */
class CafeItem {

    /**
     * ID for table item in DB
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name of item
     *
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * Short description of an item
     *
     * @var string
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * Price of item
     *
     * @var float
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * item constructor.
     *
     * @param $name
     * @param $description
     * @param $price
     */
    public function __construct($name, $description, $price) {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price) {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getPrice() {
        return empty($this->price) ? 0 : $this->price;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
