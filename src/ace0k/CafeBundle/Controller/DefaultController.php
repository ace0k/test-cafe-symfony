<?php

namespace ace0k\CafeBundle\Controller;

use ace0k\CafeBundle\Form\CreateItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ace0k\CafeBundle\Entity\CafeItem;
use ace0k\CafeBundle\Entity\CafeDrink;

class DefaultController extends Controller
{
    public function indexAction()
    {

        // @TODO: Remove this when all will be done

        $water = new CafeItem('Water', 'Water is a transparent and nearly colorless chemical substance that is the main constituent of Earth\'s streams, lakes, and oceans, and the fluids of most living organisms.', 0.1);
        $coffee_beans = new CafeItem('Coffee bean', 'A coffee seed, commonly called coffee bean, is a seed of the coffee plant, and is the source for coffee.', 0.4);
        $coffee = new CafeDrink('Coffee', 'Coffee is a brewed drink prepared from roasted coffee beans, which are the seeds of berries from the Coffea plant.', array($water, $coffee_beans));

        return $this->render('CafeBundle:Default:index.html.twig', array(
            'coffee' => $coffee
        ));
    }
}
