<?php

namespace ace0k\CafeBundle\Controller;

use ace0k\CafeBundle\Entity\CafeDrink;
use ace0k\CafeBundle\Entity\CafeItem;
use ace0k\CafeBundle\Form\CreateDrink;
use ace0k\CafeBundle\Form\CreateItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    /**
     * controller for create item page
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function itemAction(Request $request)
    {

        $item = '';
        // @TODO: create item from form and store it in DB
        $name = '';
        $description = '';
        $price = 0.0;

        if ((!empty($name)) && (!empty($description)) && (!empty($price))) {
            $item = new CafeItem($name, $description, $price);
            $form = $this->createForm(CreateItem::class, $item);
        } else {
            $form = $this->createForm(CreateItem::class);
        }

        return $this->render('CafeBundle:Page:item.html.twig', array(
            'item' => $item,
            'form' => $form->createView(),
        ));
    }

    /**
     * controller for creating drink page
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function drinkAction(Request $request)
    {
        $item = '';
        // @TODO: create drink from form and store it in DB
        $name = '';
        $description = '';
        $recipe = array();

        if ((!empty($name)) && (!empty($description)) && (!empty($recipe))) {
            $drink = new CafeDrink($name, $description, $recipe);
        }
        $form = $this->createForm(CreateDrink::class);

        return $this->render('CafeBundle:Page:drink.html.twig', array(
            'item' => $item,
            'form' => $form->createView(),
        ));
    }
}
